package tag;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.*;

public class MyTag extends TagSupport {
    private JspWriter out;
    private Map<String, String> mapUrlServlet = new TreeMap<>();

    public MyTag() {
    }

    @Override
    public int doStartTag() throws JspException {
        String url = "";
        String servletName = "";
        out = pageContext.getOut();
        ServletContext context = pageContext.getServletContext();
        Map<String, ServletRegistration> servletRegistrationMap = (Map<String, ServletRegistration>) context.getServletRegistrations();

        for (Map.Entry<String, ServletRegistration> entry : servletRegistrationMap.entrySet()) {
            servletName = entry.getKey();
            Collection<String> collection = entry.getValue().getMappings();
            Iterator<String> iterator = collection.iterator();
            while (iterator.hasNext()){
                url = iterator.next();
                mapUrlServlet.put(url,servletName);
            }
        }
        getListUrlServlet(mapUrlServlet);
        return EVAL_BODY_INCLUDE;
    }

    public void getListUrlServlet(Map<String, String> map) throws JspException {
        String servletName = "";
        String url = "";

        for (Map.Entry<String, String> stringEntry : map.entrySet()) {
            servletName = stringEntry.getValue();
            url = stringEntry.getKey();
            try {
                    out.println("<div><span>" + url + " - " + servletName + "</span></div>");
            } catch (IOException e) {
                    throw new JspException("IOExeption- " + e.toString());
            }
        }

    }
}
