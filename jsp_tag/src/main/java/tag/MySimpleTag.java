package tag;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class MySimpleTag extends SimpleTagSupport {

    private Map<String, Object> stringServletRegistrationMap = new TreeMap<>();
    private String url;

    public MySimpleTag() {
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void doTag() throws JspException {
        JspContext jspContext = getJspContext();
        try {
            PageContext pageContext = (PageContext) jspContext;
            ServletContext context = pageContext.getServletContext();
            if (context != null){
                Map<String, ServletRegistration> servletRegistrationMap = (Map<String, ServletRegistration>) context.getServletRegistrations();
                entryMap(servletRegistrationMap);
            }
            writeRez(stringServletRegistrationMap);
        }catch (ClassCastException e){
            throw new JspException("ClassCastException - " + e.toString());
        }catch (IOException e){
            throw  new JspException("IOException - " + e.toString());
        }
    }

    public void entryMap(Map<String, ServletRegistration> servletRegistrationMap){
        String urlValue = "";
        for (Map.Entry<String, ServletRegistration> entry : servletRegistrationMap.entrySet()) {
            Collection<String> collection = entry.getValue().getMappings();
            Iterator<String> iterator = collection.iterator();
            while (iterator.hasNext()){
                urlValue = iterator.next();
                if (urlValue.equals(url)){
                    stringServletRegistrationMap.put(url, entry.getValue());
                }
            }
        }
    }

    public void writeRez(Map<String, Object> map) throws IOException {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            getJspContext().getOut().print("<div><spam>" + entry.getKey() + " - " + entry.getValue().toString() + "</spam></div>");
        }
    }
}
