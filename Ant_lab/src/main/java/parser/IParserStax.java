package main.java.parser;

public interface IParserStax {

    boolean checkdepends();
    boolean checkdefaults ();
    boolean checknames ();
}
