package main.java;

import main.java.parser.IParserStax;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main extends Task implements IParserStax{

    private boolean checkdepends;
    private boolean checkdefaults;
    private boolean checknames;
    private String file_name;
    private XMLInputFactory xmlInputFactory;
    FileInputStream inputStream = null;


    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public void setCheckdepends(boolean checkdepends) {
        this.checkdepends = checkdepends;
    }

    public void setCheckdefaults(boolean checkdefaults) {
        this.checkdefaults = checkdefaults;
    }

    public void setChecknames(boolean checknames) {
        this.checknames = checknames;
    }


    @Override
    public void execute() throws BuildException {
        if (this.checkdepends != checkdepends()){
            throw new BuildException("targets with depends are used instead of \"main\" point");
        }
        if (this.checkdefaults != checkdefaults()){
            throw new BuildException("project not contains default attribute");
        }
        if (this.checknames != checknames()){
            throw new BuildException("names contains not only letters with '-'");
        }
        log(String.valueOf(checkdefaults) + "<-checkdepends");
        log(String.valueOf(checkdefaults) + "<-checkdefaults");
        log(String.valueOf(checknames) + "<-checknames");
    }

    @Override
    public boolean checkdepends() {
        xmlInputFactory = XMLInputFactory.newInstance();
        try {
            inputStream = new FileInputStream(file_name);
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(inputStream);
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    if (startElement.getName().getLocalPart().equals("target")){
                        Attribute idAttr = startElement.getAttributeByName(new QName("name"));
                        if(idAttr != null){
                            if (idAttr.getValue().equals("main")){
                                return true;
                            }
                        }
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
            log(e.getMessage());
        }
        finally {
            if (inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    log(e.getMessage() + "NO CLOSE FILE");
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkdefaults() {
        xmlInputFactory = XMLInputFactory.newInstance();
        try {
            inputStream = new FileInputStream(file_name);
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(inputStream);
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    if(startElement.getName().getLocalPart().equals("project")){
                        Attribute idAttr = startElement.getAttributeByName(new QName("default"));
                        if(idAttr != null){
                            return true;
                        }
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }finally {
            try {
                if (inputStream != null){
                    inputStream.close();
                }
            }catch (IOException e){
                log(e.getMessage() +  "NO CLOSE FILE");
            }
        }
        return false;
    }

    @Override
    public boolean checknames() {
        xmlInputFactory = XMLInputFactory.newInstance();
        try {
            inputStream = new FileInputStream(file_name);
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(inputStream);
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    if(startElement.getName().getLocalPart().equals("project")){
                        Attribute idAttr = startElement.getAttributeByName(new QName("name"));
                        if(idAttr != null){
                            if (!checkWithRegExp(idAttr.getValue())){
                                return false;
                            }
                        }
                    }
                    else if (startElement.getName().getLocalPart().equals("target")){
                        Attribute idAttr = startElement.getAttributeByName(new QName("name"));
                        if(idAttr != null){
                            if (!checkWithRegExp(idAttr.getValue())){
                                return false;
                            }
                        }
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }finally {
            try {
                if (inputStream != null){
                    inputStream.close();
                }
            }catch (IOException e){
                log(e.getMessage() + "NO CLOSE FILE");
            }
        }
        return true;
    }

    private static boolean checkWithRegExp(String userNameString){
        Pattern p = Pattern.compile("[a-z A-Z_-]*");
        Matcher m = p.matcher(userNameString);
        return m.matches();
    }


}
