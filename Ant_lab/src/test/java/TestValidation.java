package test.java;

import main.java.Main;
import org.junit.Assume;
import org.junit.Test;

public class TestValidation {

    @Test
    public void testResult(){
        Main main = new Main();
        Assume.assumeTrue(main.checkdepends());
        Assume.assumeTrue(main.checkdefaults());
        Assume.assumeTrue(main.checknames());
    }
}
