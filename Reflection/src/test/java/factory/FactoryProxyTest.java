package factory;

import annotation.Proxy;
import model.Animal;
import model.Dog;
import org.junit.Assert;
import org.junit.Test;

public class FactoryProxyTest {

    @Test
    public void checkProxyAnnotation(){
        Dog dog = new Dog();
        final Class<?> theClass = dog.getClass();
        if (theClass.isAnnotationPresent(Proxy.class)){
            final Proxy annotation = theClass.getAnnotation(Proxy.class);
            Assert.assertTrue(annotation.invocationHandler().equals("proxy.ProxyIH"));
        }
    }
}
