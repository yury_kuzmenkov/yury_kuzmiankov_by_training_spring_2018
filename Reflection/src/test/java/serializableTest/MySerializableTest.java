package serializableTest;

import exaptions.NoSerializableExaption;
import org.junit.Test;
import serializable.MySerialisable;
import model.SerializeExample;

import java.util.Map;

public class MySerializableTest {

    @Test
    public void checkAnnotation(){
        SerializeExample example = new SerializeExample();
        MySerialisable mySerialisable = new MySerialisable();
            Map<Class<SerializeExample>, String> map = mySerialisable.write(example);
            if (map.isEmpty()){
                throw new NoSerializableExaption("not annotation");
            }
    }

    @Test
    public void checkReturnObject(){
        MySerialisable serialisable = new MySerialisable();
        SerializeExample example = new SerializeExample();
        Map<Class<?>, String> map = serialisable.write(example);
        Object o =  serialisable.read(map);
        if (!(o instanceof SerializeExample)){
            throw new NoSerializableExaption("return wrong instance");
        }
    }

}
