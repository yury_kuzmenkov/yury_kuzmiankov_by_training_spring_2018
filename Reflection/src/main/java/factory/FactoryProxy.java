package factory;

import annotation.Proxy;
import proxy.ProxyIH;


public class FactoryProxy {

    public static <T> T getInstanceOf(Class<T> tClass) {
        Proxy proxy = tClass.getAnnotation(Proxy.class);
        Object o = null;
        try {
            if (proxy != null) {
                o = tClass.newInstance();
                    return (T) java.lang.reflect.Proxy.newProxyInstance(
                            o.getClass().getClassLoader(),
                            o.getClass().getInterfaces(),
                            new ProxyIH(o));
            }

        } catch (IllegalAccessException e) {
            System.out.println("IllegalAccessException " + e.getMessage());
        } catch (InstantiationException e) {
            System.out.println("InstantiationException " + e.getMessage());
        }
        try {
            o = tClass.newInstance();
        } catch (InstantiationException e) {
            System.out.println("Wrong instantiation " + e.getMessage());
        } catch (IllegalAccessException e) {
            System.out.println("IllegalAccessException " + e.getMessage());
        }
        return (T)o;
    }
}
