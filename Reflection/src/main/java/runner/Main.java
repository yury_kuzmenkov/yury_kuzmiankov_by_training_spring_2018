package runner;

import factory.FactoryProxy;
import model.Animal;
import model.Dog;
import serializable.MySerialisable;
import model.SerializeExample;

import java.util.Map;

public class Main {

    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        Animal animal = FactoryProxy.getInstanceOf(Dog.class);

        SerializeExample example = new SerializeExample("Petr", 30, "Avenu");

        MySerialisable mySerialisable = new MySerialisable();

        Map<Class<SerializeExample>, String> map = mySerialisable.write(example);

        SerializeExample serializeExample = (SerializeExample) mySerialisable.read(map);

    }
}
