package model;

import annotation.Proxy;

@Proxy(invocationHandler = "proxy.ProxyIH")
public class Dog implements Animal {

    private int age;
    private String name;
    private String par;

    public Dog() {
    }

    public Dog(int age, String name, String par) {
        this.age = age;
        this.name = name;
        this.par = par;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPar() {
        return par;
    }

    public void setPar(String par) {
        this.par = par;
    }

    public void getVoice() {
        System.out.println("Ah-ah");
    }

    @Override
    public String toString() {
        return "Dog{}";
    }
}
