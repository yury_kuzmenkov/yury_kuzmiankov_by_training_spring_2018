package model;

import annotation.Proxy;

@Proxy(invocationHandler = "model.Dog")
public class DogProxy implements Animal {


    public DogProxy() {
    }

    public void getVoice() {
        System.out.println("get Voice from proxy");
    }
}
