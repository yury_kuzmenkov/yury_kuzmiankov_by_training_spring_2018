package model;

import annotation.Serializable;

@Serializable
public class SerializeExample {

    @Serializable(alias = "aliasNameFiled")
    private String name;

    @Serializable
    private int age;

    private String address;

    public SerializeExample() {
    }

    public SerializeExample(String name, int age, String address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "SerializeExample{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", address='" + address + '\'' +
                '}';
    }
}
