package serializable;

import constants.ConstantType;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class MySerialisable<T> {

    final String delimiterKayValue = ":";

    final String delimiterRez = ",";

    /**
     * @param object accepted Object for serializable
     * @return serializable map <Class<T>, String> where String is json type from field

     */

    public Map<Class<T>, String> write(T object)  {

        Map<Class<T>, String> map = new HashMap<>();

        StringBuilder builder = new StringBuilder();

        Class aClass = object.getClass();

        annotation.Serializable serializable = (annotation.Serializable) aClass.getAnnotation(annotation.Serializable.class);

        if (serializable != null){
            Field[] fields = aClass.getDeclaredFields();
            if (fields.length > 0){
                for (Field field : fields) {
                    field.setAccessible(true);
                    Object value = null;
                    try {
                        value = field.get(object);
                    } catch (IllegalAccessException e) {
                        System.out.println(e.getMessage() + "");
                        e.printStackTrace();
                    }
                    String key = field.getName();
                    serializable = field.getAnnotation(annotation.Serializable.class);
                    if (serializable != null){
                        builder.append(key + delimiterKayValue + value + delimiterRez);
                    }
                }
            }
            map.put(aClass, builder.toString());
        }
        return map;
    }

    /**
     *
     * @param map accepted map where key your Serializable class, value String like json type from Serializable class fields.
     * @param <T> type which was Serializable
     * @return deserializable object
     */

    public <T>T read(Map<Class<T>, String> map){
        Class aClass;
        T returnObject = null;
        final String GET = "get";
        final String SET  = "set";
        if (!map.isEmpty()){
            for (Map.Entry<Class<T>, String> entry : map.entrySet()) {
                aClass = entry.getKey();
                String value = entry.getValue();
                String[] parsValue = value.split(delimiterRez);

                try {
                    returnObject = (T)aClass.newInstance();
                } catch (InstantiationException e) {
                    System.out.println("this {@code Class} represents an abstract class," +
                            "an interface, an array class, a primitive type, or void; " + e.getMessage());
                } catch (IllegalAccessException e) {
                    System.out.println("the class or its nullary constructor is not accessible. " + e.getMessage());
                }

                for (String pairKayValue : parsValue) {
                    String[] keyValue = pairKayValue.split(delimiterKayValue);
                    try {
                        Field field = aClass.getDeclaredField(keyValue[0]);
                        field.setAccessible(true);
                        String fieldName = field.getName();
                        fieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1).toLowerCase();
                        String getN = GET+fieldName;
                        String setN = SET+fieldName;
                        Method get = aClass.getDeclaredMethod(getN, null);
                        Method set = aClass.getDeclaredMethod(setN, get.getReturnType());

                        if (returnObject != null){
                            final String VALUE = keyValue[1];
                            String returnType = get.getReturnType().toString();
                            Object writeValue = checkType(VALUE, returnType);
                            set.invoke(returnObject, writeValue);
                                          }

                    } catch (NoSuchFieldException e) { System.out.println("NoSuchFieldException " + e.getMessage());}
                      catch (NoSuchMethodException e) { System.out.println("NoSuchMethodException " + e.getMessage());}
                      catch (IllegalArgumentException e){ System.out.println("IllegalArgumentException " + e.getMessage());}
                      catch (InvocationTargetException e) { System.out.println("InvocationTargetException " + e.getMessage());}
                      catch (IllegalAccessException e) {System.out.println("IllegalAccessException " + e.getMessage());}
                }
            }
        }
        return (T)returnObject;
    }

    private Object checkType(String value, String returnType){
        Object writeValue = null;
        switch (returnType){
            case ConstantType.BYTE : writeValue = new Byte(value);
                break;
            case ConstantType.SHORT : writeValue = new Short(value);
                break;
            case ConstantType.INT : writeValue = new Integer(value);
                break;
            case ConstantType.FLOAT : writeValue = new Float(value);
                break;
            case ConstantType.DOUBLE : writeValue = new Double(value);
                break;
            case ConstantType.BOOLEAN : writeValue = new Boolean(value);
                break;
            case ConstantType.STRING : writeValue = new String(value);
                break;
        }
        return writeValue;
    }

}
