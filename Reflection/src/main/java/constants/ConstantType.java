package constants;

public interface ConstantType {
     String BYTE = "byte";
     String SHORT = "short";
     String INT = "int";
     String FLOAT = "float";
     String DOUBLE = "double";
     String BOOLEAN = "boolean";
     String STRING = "class java.lang.String";
}
