package proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;


public class ProxyIH implements InvocationHandler {

    private Object target;

    public ProxyIH() {}

    public ProxyIH(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result;
        result = method.invoke(target, args);
        return result;
    }
}
