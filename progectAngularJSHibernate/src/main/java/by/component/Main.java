package by.component;

import by.component.configuration.AppConfig;
import by.component.model.Role;
import by.component.model.User;
import by.component.servic.IServicRole;
import by.component.servic.IServicUser;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import java.util.List;

public class Main {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {

        AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        IServicUser serviceUser = (IServicUser) context.getBean("service");
        IServicRole serviceRole = (IServicRole) context.getBean("serviceRole");

        List<User> allUsers = serviceUser.findAllUsers();
        allUsers.stream().forEach(x-> System.out.println(x));

        Role role = new Role();
        role.setRole("ROLE_MASTER");
//        serviceRole.saveRole(role);

        List<Role> allRoles = serviceRole.findAllRoles();
        allRoles.stream().forEach(x -> System.out.println(x));

        serviceUser.deleteUserByName("Sam");

        List<User> allUsersNext = serviceUser.findAllUsers();
        allUsersNext.stream().forEach(x-> System.out.println(x));

        context.close();

    }
/***/
}
