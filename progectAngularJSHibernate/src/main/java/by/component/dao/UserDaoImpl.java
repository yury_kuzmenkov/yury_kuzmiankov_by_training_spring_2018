package by.component.dao;

import by.component.model.Role;
import by.component.model.User;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userDAO")
public class UserDaoImpl extends AbstractDao implements IUserDao {
    @Override
    public void saveUser(User user) {
        persist(user);
    }

    @SuppressWarnings("unchecked")
    public List<User> findAllUsers() {
        Criteria criteria = getSession().createCriteria(User.class);
        return (List<User>) criteria.list();
    }

    @Override
    public void deleteUserByName(String name) {
        Query query = getSession().createSQLQuery("delete from users where name = :name");
        query.setString("name", name);
        query.executeUpdate();
    }

    @Override
    public User findByName(String name) {
        return null;
    }

    @Override
    public void updateUser(User user) {

    }
}
