package by.component.dao;

import by.component.model.Role;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("roleDAO")
public class RoleDaoImpl extends AbstractDao implements IRoleDao {
    @Override
    public void saveRole(Role role) {
        persist(role);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Role> findAllRoles() {
        Criteria criteria = getSession().createCriteria(Role.class);
        return (List<Role>) criteria.list();
    }

    @Override
    public void deleteRoleByName(String name) {
        Query query = getSession().createSQLQuery("delete from roles where role = :role");
        query.setString("role", name);
        query.executeUpdate();
    }
}
