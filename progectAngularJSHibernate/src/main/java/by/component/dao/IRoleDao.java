package by.component.dao;

import by.component.model.Role;

import java.util.List;

public interface IRoleDao {

    void saveRole(Role role);

    List<Role> findAllRoles();

    void deleteRoleByName(String name);
}
