package by.component.dao;

import by.component.model.Role;
import by.component.model.User;

import java.util.List;

public interface IUserDao {

    void saveUser(User user);

    List<User> findAllUsers();

    void deleteUserByName(String name);

    User findByName(String name);

    void updateUser(User user);
}
