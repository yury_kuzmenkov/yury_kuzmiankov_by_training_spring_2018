package by.component.servic;

import by.component.dao.IRoleDao;
import by.component.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("serviceRole")
@Transactional
public class ServicRoleImpl implements IServicRole {

    @Autowired
    private IRoleDao roleDao;

    @Override
    public void saveRole(Role role) {
        roleDao.saveRole(role);
    }

    @Override
    public List<Role> findAllRoles() {
        return roleDao.findAllRoles();
    }

    @Override
    public void deleteUserByName(String name) {
        roleDao.deleteRoleByName(name);
    }
}
