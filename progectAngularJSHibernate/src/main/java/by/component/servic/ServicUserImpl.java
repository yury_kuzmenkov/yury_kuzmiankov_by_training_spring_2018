package by.component.servic;

import by.component.dao.IUserDao;
import by.component.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("service")
@Transactional
public class ServicUserImpl implements IServicUser {

    @Autowired
    private IUserDao userDAO;

    @Override
    public void saveUser(User user) {
        userDAO.saveUser(user);
    }

    @Override
    public List<User> findAllUsers() {
        return userDAO.findAllUsers();
    }

    @Override
    public void deleteUserByName(String name) {
        userDAO.deleteUserByName(name);
    }

    @Override
    public User findByName(String name) {
        return userDAO.findByName(name);
    }

    @Override
    public void updateUser(User user) {
        userDAO.updateUser(user);
    }
}
