package by.component.servic;

import by.component.model.Role;

import java.util.List;

public interface IServicRole {

    void saveRole(Role role);

    List<Role> findAllRoles();

    void deleteUserByName(String name);
}
