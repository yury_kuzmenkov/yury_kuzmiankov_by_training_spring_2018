package building.model;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

public class Elevator {

    private Queue<Passenger> elevatorContainer;

    public Elevator(int elevatorCapacity) {
        this.elevatorContainer = new ArrayBlockingQueue<Passenger>(elevatorCapacity);
    }

    public Queue<Passenger> getElevatorContainer() {
        return elevatorContainer;
    }

    public void setElevatorContainer(Queue<Passenger> elevatorContainer) {
        this.elevatorContainer = elevatorContainer;
    }

}

