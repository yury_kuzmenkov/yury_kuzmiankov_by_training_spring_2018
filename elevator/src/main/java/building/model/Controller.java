package building.model;

import building.constants.ConstantsLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;
import java.util.Map;
import java.util.Queue;

public class Controller {

    static final private Logger logger = LogManager.getLogger(Controller.class.getName());
    private Building building;

    public Controller(Building building) {
        this.building = building;
    }

    public void startElevator() throws InterruptedException {
        while (!isEmptyDispatchStory() || !isEmptyElevator()){
            goUp(building.getStoriesNumber());
            goDown(building.getStoriesNumber());
        }
    }

    private boolean isEmptyDispatchStory(){
        for (Map.Entry<Integer, Storey> entry : building.getStoreysMap().entrySet()) {
            Storey storey = entry.getValue();
            Queue<Passenger> queue = storey.getDispatchStoryContainer();
            if (!queue.isEmpty()){
                return false;
            }
        }
        return true;
    }

    private boolean isEmptyElevator(){
        return building.getElevator().getElevatorContainer().isEmpty() ? true : false;
    }

    private void goUp(int storeNumbers){
        for (int i = 1; i < storeNumbers; i++) {
            logger.trace(ConstantsLog.MOVING_ELEVATOR + i + ConstantsLog.TO_STOREY + (i + 1));
            if (isPassengerInElevator(building.getElevator().getElevatorContainer())){
                 sayOut(i, building.getElevator().getElevatorContainer(),
                         building.getStoreyByNumber(i).getArrivalStoryContainer());
            }
            sayInn(storeNumbers, building.getElevator().getElevatorContainer(),
                    building.getStoreyByNumber(i).getDispatchStoryContainer());
        }
    }

    private boolean askDestination(int passengerDest, int destinyElevator){
        if (destinyElevator >= passengerDest){
            return true;
        }
        return false;
    }


    private void goDown(int storeNumbers){
        final int firstStorey = 1;
        for (int i = storeNumbers; i > 1 ; i--) {
            logger.trace(ConstantsLog.MOVING_ELEVATOR + i + ConstantsLog.TO_STOREY + (i - 1));
            if (isPassengerInElevator(building.getElevator().getElevatorContainer())){
                sayOut(i, building.getElevator().getElevatorContainer(),
                        building.getStoreyByNumber(i).getArrivalStoryContainer());
            }
            sayInn(firstStorey, building.getElevator().getElevatorContainer(),
                    building.getStoreyByNumber(i).getDispatchStoryContainer());
        }
    }

    private void sayInn(int destiny , Queue<Passenger> elevatorContainer, Queue<Passenger> dispatchStoryContainer){
        Iterator<Passenger> iterator = dispatchStoryContainer.iterator();
        Passenger passenger;
        while (iterator.hasNext()){
            passenger = iterator.next();
            if (askDestination(passenger.getDestinationStory(), destiny)){
                if (inviteInElevator(elevatorContainer, passenger)){
                    iterator.remove();
                }
            }else {
                if (inviteInElevator(elevatorContainer, passenger)){
                    iterator.remove();
                }
            }
        }
    }
  private boolean inviteInElevator(Queue<Passenger> elevatorCont, Passenger passenger){
        if (elevatorCont.offer(passenger)){
            building.getTransportationTask(passenger).changeStateInProgress();
            return true;
        }else {return false;}
  }

    private void sayOut(int storeyNumber, Queue<Passenger> elevatorContainer, Queue<Passenger> arrivalStoryContainer){
        Iterator<Passenger> iterator = elevatorContainer.iterator();
        Passenger passenger;
        while (iterator.hasNext()){
            passenger = iterator.next();
            if (passenger.getDestinationStory() == storeyNumber){
                if (arrivalStoryContainer.offer(passenger)) synchronized (passenger){
                    building.getTransportationTask(passenger).changeStateComplete();
                    logger.trace(ConstantsLog.DEBOADING_OF_PASSENGER + " " +
                            passenger.getPassengerID() + ConstantsLog.TO_STOREY + storeyNumber);
                }
                iterator.remove();
            }
        }
    }

    private boolean isPassengerInElevator(Queue<Passenger> passengerList){
       if (!passengerList.isEmpty()){
           return true;
       }else {
           return false;
       }
    }
}

