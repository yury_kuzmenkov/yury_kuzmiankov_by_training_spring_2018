package building.model;


import building.constants.ConstantsLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TransportationTask implements Runnable {

    static final Logger logger = LogManager.getLogger(TransportationTask.class.getName());
    private Passenger passenger;

    public TransportationTask(Passenger passenger) {
        this.passenger = passenger;
        this.passenger.setTransportationState(TransportationState.IN_PROGRESS);

    }

    public void changeStateComplete(){
        passenger.setTransportationState(TransportationState.COMPLETED);
        this.passenger.notify();
        logger.trace(ConstantsLog.COMPLETION_TRANSPORTATION);
    }

    public void changeStateAborted(){
        passenger.setTransportationState(TransportationState.ABORTED);
        this.passenger.notify();
        logger.trace(ConstantsLog.ABORTING_TRANSPORTATION);
    }

    public void changeStateInProgress(){
        passenger.setTransportationState(TransportationState.IN_PROGRESS);
    }

    public Passenger getPassenger() {
        return this.passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }


    @Override
    public void run() {
        getPassenger().createPassengerThread();
    }

}
