package building.model;


import building.constants.ConstantsLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class Storey {
    static final private Logger logger = LogManager.getLogger(Storey.class.getName());
    private Queue<Passenger> arrivalStoryContainer;
    private Queue<Passenger> dispatchStoryContainer;

    public Storey() {
        this.arrivalStoryContainer = new LinkedBlockingQueue<Passenger>();
        this.dispatchStoryContainer = new LinkedBlockingQueue<Passenger>();
    }

    public Queue<Passenger> getArrivalStoryContainer() {
        return arrivalStoryContainer;
    }

    public void setArrivalStoryContainer(Queue<Passenger> arrivalStoryContainer) {
        this.arrivalStoryContainer = arrivalStoryContainer;
    }

    public Queue<Passenger> getDispatchStoryContainer() {
        return dispatchStoryContainer;
    }

    public void setDispatchStoryContainer(Queue<Passenger> dispatchStoryContainer) {
        this.dispatchStoryContainer = dispatchStoryContainer;
    }

    public boolean setPassInDispatch(Passenger pass, int storey) {
        if (getDispatchStoryContainer().offer(pass)){
            logger.trace(ConstantsLog.BOADING_OF_PASSENGER + pass.getPassengerID() + ConstantsLog.TO_STOREY + storey);
            return true;
        }else {
            return false;
        }
    }

}
