package building.model;


import building.constants.ConstantsLog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Passenger {
    static final Logger logger = LogManager.getLogger(Passenger.class.getName());
    private int passengerID;
    private int destinationStory;
    private TransportationState transportationState;

    public Passenger(int passengerID, int destinationStory) {
        this.passengerID = passengerID;
        this.destinationStory = destinationStory;
        this.transportationState = TransportationState.NOT_STARTED;
        logger.trace(ConstantsLog.STARTING_TRANSPORTATION);
    }

    public synchronized void createPassengerThread(){
        try {
            while (getTransportationState() != TransportationState.ABORTED && getTransportationState() != TransportationState.COMPLETED){
                this.wait();
            }
        } catch (InterruptedException e) {
            logger.error("Exception throw", e);

        }
    }

    public int getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(int passengerID) {
        this.passengerID = passengerID;
    }

    public int getDestinationStory() {
        return destinationStory;
    }

    public void setDestinationStory(int destinationStory) {
        this.destinationStory = destinationStory;
    }

    public TransportationState getTransportationState() {
        return transportationState;
    }

    public void setTransportationState(TransportationState transportationState) {
        this.transportationState = transportationState;
    }

}
