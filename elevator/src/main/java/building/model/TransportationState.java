package building.model;

public enum TransportationState {
    NOT_STARTED, IN_PROGRESS, COMPLETED, ABORTED;
}
