package building.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class Building {
    static final Logger logger = LogManager.getLogger(Building.class.getName());
    private int passengersNumber;
    private Map<Integer, Storey> storeysMap = new HashMap<Integer, Storey>();
    private Elevator elevator;
    private int storiesNumber;
    private Map<Passenger, TransportationTask> transportationTaskMap = new HashMap<Passenger, TransportationTask>();

    public Building() {
    }

    public Building(int storiesNumber, int elevatorCapacity, int passengersNumber) throws InterruptedException {
        this.storiesNumber = storiesNumber;
        this.passengersNumber = passengersNumber;
        this.storeysMap = setStoreysNumbers(storiesNumber);
        this.elevator = new Elevator(elevatorCapacity);
        setPassengersOnStoreys(storiesNumber, passengersNumber);
        startPassengersThreads();
    }

    public int getPassengersNumber() {
        return passengersNumber;
    }

    public void setPassengersNumber(int passengersNumber) {
        this.passengersNumber = passengersNumber;
    }

    public int getStoriesNumber() {
        return storiesNumber;
    }

    public void setStoriesNumber(int storiesNumber) {
        this.storiesNumber = storiesNumber;
    }

    public Map<Integer, Storey> getStoreysMap() {
        return this.storeysMap;
    }

    public void setStoreysMap(Map<Integer, Storey> storeysMap) {
        this.storeysMap.putAll(storeysMap);
    }

    public Elevator getElevator() {
        return elevator;
    }

    public void setElevator(Elevator elevator) {
        this.elevator = elevator;
    }


    private Map<Integer, Storey> setStoreysNumbers(int storeysNumbers){
        for (int i = 1; i <= storeysNumbers; i++) {
            getStoreysMap().put(i, new Storey());
        }
        return getStoreysMap();
    }

    private int getRandomNumberInRange(int storeysNumbers) {
        final int FOR_SET_STOREY = 1;
        Random r = new Random();
        return r.nextInt(storeysNumbers) + FOR_SET_STOREY;
    }

    private int getDestination(int realStorey, int storeyNumbers){
        int destiny = 0;
        do {
            destiny = getRandomNumberInRange(storeyNumbers);
        }
        while (destiny == realStorey);
        return destiny;
    }

    public void setPassengersOnStoreys(int storeysNumbers, int passengerNumber){
        int passengerId = passengerNumber;
        while (passengerNumber != 0){
            for (int i = 1; i <= storeysNumbers; i++) {
                Storey storey = getStoreysMap().get(i);
                if (storey == null){
                    setStoreysNumbers(storeysNumbers);
                    storey = getStoreysMap().get(i);
                }
                if (passengerNumber != 0){
                    try {
                        storey.setPassInDispatch(new Passenger(passengerId--, getDestination(i, storeysNumbers)), i);
                        getStoreysMap().put(i, storey);
                        passengerNumber--;
                    }catch (IllegalStateException e){
                        logger.error("Exception throw", e);
                    }
                }else {
                    break;
                }
            }
        }
    }

    public Storey getStoreyByNumber (int storeyNumber){
        return getStoreysMap().get(storeyNumber);
    }


     public Queue<Passenger> getAllPassengerDC() {
        Queue<Passenger> passengerQueue = new LinkedBlockingQueue<Passenger>();
         for (Map.Entry<Integer, Storey> entry : storeysMap.entrySet()) {
             Storey storey = entry.getValue();
             passengerQueue.addAll(storey.getDispatchStoryContainer());
         }
     return passengerQueue;
     }

     public void startPassengersThreads() throws InterruptedException {
         for (Passenger passenger : getAllPassengerDC()) {
             TransportationTask transportationTaskV = new TransportationTask(passenger);
             transportationTaskMap.put(passenger, transportationTaskV);
             new Thread(transportationTaskV).start();
         }

     }

    public Map<Passenger, TransportationTask> getTransportationTaskMap() {
        return transportationTaskMap;
    }

    public TransportationTask getTransportationTask(Passenger passenger){
        TransportationTask transportationTask = null;
        for (Map.Entry<Passenger, TransportationTask> entry : getTransportationTaskMap().entrySet()) {
            if (passenger.equals(entry.getKey())){
                transportationTask = entry.getValue();
            }
        }
        return transportationTask;
    }
}
