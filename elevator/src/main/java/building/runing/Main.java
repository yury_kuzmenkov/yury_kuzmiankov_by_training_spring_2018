package building.runing;

import building.constants.ConstantPath;
import building.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Properties properties = new Properties();
        String path = new File(ConstantPath.PATH_TO_PROPERTY)
                .getAbsolutePath();
        try {
            properties.load(new FileReader(path));

        final int passengersNumber = Integer.valueOf(properties.getProperty(ConstantPath.PASSENGER_NUMBER));
        final int elevatorCapacity = Integer.valueOf(properties.getProperty(ConstantPath.ELEVATOR_CAPACITY));
        final int storiesNumber = Integer.valueOf(properties.getProperty(ConstantPath.STORIES_NUMBER));

        Building building = new Building(storiesNumber, elevatorCapacity, passengersNumber);
        Controller controller = new Controller(building);
        controller.startElevator();

        } catch (InterruptedException e) {
            logger.error("Exception created Passenger threads", e);

        }catch (IOException ex){
            logger.error("Read property file Exception", ex);
        }
    }
}
