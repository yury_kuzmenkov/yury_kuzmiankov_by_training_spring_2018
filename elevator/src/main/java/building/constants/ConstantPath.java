package building.constants;

public class ConstantPath {

    public static final String PATH_TO_PROPERTY = "src/main/resources/config.properties";
    public static final String PASSENGER_NUMBER = "passengersNumber";
    public static final String ELEVATOR_CAPACITY = "elevatorCapacity";
    public static final String STORIES_NUMBER = "storiesNumber";
}
