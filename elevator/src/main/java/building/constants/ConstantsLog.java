package building.constants;

public class ConstantsLog {

     public static final String STARTING_TRANSPORTATION = "STARTING_TRANSPORTATION ";
     public static final String COMPLETION_TRANSPORTATION = "COMPLETION_TRANSPORTATION ";
     public static final String ABORTING_TRANSPORTATION = "ABORTING_TRANSPORTATION ";
     public static final String MOVING_ELEVATOR = "MOVING_ELEVATOR from storey - ";
     public static final String BOADING_OF_PASSENGER = "BOADING_OF_PASSENGER ";
     public static final String DEBOADING_OF_PASSENGER = "DEBOADING_OF_PASSENGER ";
     public static final String TO_STOREY = " to storey - ";
}
