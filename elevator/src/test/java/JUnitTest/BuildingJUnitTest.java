package JUnitTest;

import building.model.Building;
import building.model.Storey;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class BuildingJUnitTest {

    @Mock
    Building buildingMock;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void getPassengerOnStoreyTest(){
        Building building = new Building();
        final int STOREY_NUMBER_ONE = 1;
        final int STOREY_NUMBER_TWO = 2;
        final int STOREY_NUMBER_THREE = 3;
        Map<Integer, Storey> storeyMap = new HashMap<Integer, Storey>();
        Storey storeyOne = mock(Storey.class);
        Storey storeyTwo = mock(Storey.class);
        Storey storeyThree = mock(Storey.class);
        storeyMap.put(STOREY_NUMBER_ONE, storeyOne);
        storeyMap.put(STOREY_NUMBER_TWO, storeyTwo);
        storeyMap.put(STOREY_NUMBER_THREE, storeyThree);
        building.setStoreysMap(storeyMap);
        assertSame(storeyOne, building.getStoreysMap().get(1));
    }

    @Test
    public void setStoreyTest() throws NoSuchMethodException {
        Building building = new Building();
        final int setStoreyNumber = 3;
        final String SET_STOREYS_NUMBER = "setStoreysNumbers";
        Method setStorey = Building.class.getDeclaredMethod(SET_STOREYS_NUMBER, int.class);
        setStorey.setAccessible(true);
        try {
           Map<Integer, Storey> mapStoreys = (Map<Integer, Storey>) setStorey.invoke(building, new Integer(setStoreyNumber));
           assertTrue(setStoreyNumber == mapStoreys.size());
           assertTrue(mapStoreys.get(setStoreyNumber) != null);
        } catch (IllegalAccessException e) {
            System.out.println("method is an instance method and the specified object argument" +
                    "is not an instance of the class or interface declaring the underlying method");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            System.out.println("the underlying method throws an exception");
            e.printStackTrace();
        }
    }

    @Test
    public void getRandomNumberInRangeTest(){
        Method getRandomNumberInRange = null;
        try {
            final int setStoreysNumber = 5;
            final String GET_RANDOM = "getRandomNumberInRange";
            getRandomNumberInRange = Building.class.getDeclaredMethod(GET_RANDOM, int.class);
            getRandomNumberInRange.setAccessible(true);
            Integer randomNumber = (Integer) getRandomNumberInRange.invoke(buildingMock, new Integer(setStoreysNumber));
            assertTrue(randomNumber <= setStoreysNumber);
        } catch (NoSuchMethodException e) {
            System.out.println("matching method is not found");
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            System.out.println("method is an instance method and the specified object argument" +
                    "is not an instance of the class or interface declaring the underlying method");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            System.out.println("the underlying method throws an exception");
            e.printStackTrace();
        }
    }

    @Test
    public void getDestinationTest(){
        Method getDestination = null;
        final int realStorey = 1;
        final int storeyNumbers = 5;
        final String GET_DESTINATION = "getDestination";
        try {
            getDestination = Building.class.getDeclaredMethod(GET_DESTINATION, int.class, int.class);
            getDestination.setAccessible(true);
            Integer randomNumber = (Integer) getDestination.invoke(buildingMock, new Integer(realStorey), new Integer(storeyNumbers));
            assertTrue(randomNumber != realStorey && randomNumber <= storeyNumbers);
        } catch (NoSuchMethodException e) {
            System.out.println("matching method is not found");
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            System.out.println("method is an instance method and the specified object argument " +
                    "is not an instance of the class or interface declaring the underlying method");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            System.out.println("the underlying method throws an exception");
            e.printStackTrace();
        }
    }

    @Test
    public void setPassengersOnStoreysTest(){
        Building building = new Building();
        final int storeyOne = 1;
        final int storeyTwo = 2;
        final int storeysNumber = 2;
        final int passengerNumber = 2;
        building.setPassengersOnStoreys(storeysNumber, passengerNumber);
        /**
         * check that there are Passengers on Dispatch Story Container
         */
        assertTrue(building.getAllPassengerDC().size() != 0);
        /**
         * check that Arrival Storey Container on all storeys is empty.
         */
        assertTrue(building.getStoreysMap().get(storeyOne).getArrivalStoryContainer().size() == 0);
        assertTrue(building.getStoreysMap().get(storeyTwo).getArrivalStoryContainer().size() == 0);
    }

    @Test
    public void getStoreyByNumberTest() {
        Building building = new Building();
        Map<Integer, Storey> storeyMap = new HashMap<Integer, Storey>();
        final int numberStoreyOne = 1;
        final int numberStoreyTwo = 2;
        Storey storeyOne = mock(Storey.class);
        Storey storeyTwo = mock(Storey.class);
        storeyMap.put(numberStoreyOne, storeyOne);
        storeyMap.put(numberStoreyTwo, storeyTwo);
        building.setStoreysMap(storeyMap);
        assertSame(storeyOne, building.getStoreyByNumber(numberStoreyOne));
        assertSame(storeyTwo, building.getStoreyByNumber(numberStoreyTwo));
    }

    @Test
    public void getAllPassengerDCTest(){
        Building building = new Building();
        final int storeysNumber = 2;
        final int passengerNumber = 2;
        building.setPassengersOnStoreys(storeysNumber, passengerNumber);
        assertTrue(building.getAllPassengerDC().size() == passengerNumber);
    }
}
