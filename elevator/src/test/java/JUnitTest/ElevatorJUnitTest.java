package JUnitTest;

import building.model.Elevator;
import building.model.Passenger;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Queue;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ElevatorJUnitTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Captor
    private ArgumentCaptor<Queue<Passenger>> captor;

    @Test
    public void checkSize(){
        Elevator elevator = new Elevator(2);
        assertEquals(true, elevator.getElevatorContainer().isEmpty());
        for (int i = 1; i < 5; i++) {
            elevator.getElevatorContainer().offer(new Passenger(i, 9));
        }
        assertEquals(2, elevator.getElevatorContainer().size());
    }

    @Test
    public void checkElementCapacity(){
        final int capacityNumber = 2;
        Elevator elevator = new Elevator(capacityNumber);

        Passenger passengerOne = mock(Passenger.class);
        Passenger passengerTwo = mock(Passenger.class);
        Passenger passengerTree = mock(Passenger.class);

        elevator.getElevatorContainer().offer(passengerOne);
        elevator.getElevatorContainer().offer(passengerTwo);
        elevator.getElevatorContainer().offer(passengerTree);

        assertEquals(2, elevator.getElevatorContainer().size());
    }

    @Test
    public void isContainValue(){
        final int capacityNumber = 2;
        Elevator elevator = new Elevator(capacityNumber);
        Passenger passengerOne = mock(Passenger.class);
        Passenger passengerTwo = mock(Passenger.class);
        Passenger passengerThree = mock(Passenger.class);
        elevator.getElevatorContainer().offer(passengerOne);
        elevator.getElevatorContainer().offer(passengerTwo);
        elevator.getElevatorContainer().offer(passengerThree);

        Queue<Passenger> queue = elevator.getElevatorContainer();
        Queue<Passenger> queueMock = mock(Queue.class);
        queueMock.addAll(queue);

        verify(queueMock).addAll(captor.capture());
        final Queue<Passenger> capturedArgument = captor.getValue();
        assertNotEquals(passengerThree, capturedArgument);
        assertThat(capturedArgument, hasItem(passengerOne));
    }
}
