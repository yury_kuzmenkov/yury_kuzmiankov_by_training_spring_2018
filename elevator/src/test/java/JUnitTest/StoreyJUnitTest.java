package JUnitTest;

import building.model.Passenger;
import building.model.Storey;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;
import static org.mockito.Mockito.*;

public class StoreyJUnitTest {

    @Captor
    private ArgumentCaptor<Queue<Passenger>> captor;

    @Test
    public void createStoreyTest(){
        Storey storey = mock(Storey.class);
        Queue<Passenger> passengerQueue = new LinkedBlockingQueue<Passenger>();

        when(storey.getDispatchStoryContainer()).thenReturn(passengerQueue);
        when(storey.getArrivalStoryContainer()).thenReturn(passengerQueue);

        assertEquals(storey.getDispatchStoryContainer(), passengerQueue);
        assertEquals(storey.getArrivalStoryContainer(), passengerQueue);
    }

    @Test
    public void setPassengerTest(){
        Storey storey = mock(Storey.class);
        Passenger passenger = mock(Passenger.class);
        final int storeyNumber = 8;

        when(storey.setPassInDispatch(passenger, storeyNumber)).thenReturn(true);
        assertEquals(storey.setPassInDispatch(passenger, storeyNumber), true);
        verify(storey).setPassInDispatch(passenger, storeyNumber);
    }

    @Test
    public void checkAddPassenger(){
        final int storeyNumber = 1;
        Storey storey = new Storey();
        Passenger passenger = mock(Passenger.class);
        storey.setPassInDispatch(passenger, storeyNumber);

        Queue<Passenger> queueDispatch = storey.getDispatchStoryContainer();
        Queue<Passenger> queueArrival = storey.getArrivalStoryContainer();

        assertEquals(queueDispatch, storey.getDispatchStoryContainer());
        assertEquals(queueArrival, storey.getArrivalStoryContainer());
    }
}
