package JUnitTest;

import building.model.Passenger;
import building.model.TransportationState;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PassengerJUnitTest {

    @Test
    public void createPassengerTest(){
        Passenger passenger = mock(Passenger.class);

        when(passenger.getDestinationStory()).thenReturn(5);
        when(passenger.getPassengerID()).thenReturn(99);
        when(passenger.getTransportationState()).thenReturn(TransportationState.NOT_STARTED);

        assertEquals(passenger.getDestinationStory(), 5);
        assertEquals(passenger.getPassengerID(), 99);
        assertEquals(passenger.getTransportationState(), TransportationState.NOT_STARTED);
    }
}
