package junitTest.dao;

import java.util.Map;
import java.util.TreeMap;

public interface IDao {

   Map<String, String> MAP = new TreeMap<>();

   String putK(String k, String v);

   String getV(String k);


}
