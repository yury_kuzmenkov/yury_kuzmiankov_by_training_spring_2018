package junitTest.model;

import junitTest.dao.IDao;


public class DaoImpl implements IDao {


    @Override
    public String putK(String k, String v) {
        return MAP.put(k, v);
    }

    @Override
    public String getV(String k) {
        return MAP.get(k);
    }


}
