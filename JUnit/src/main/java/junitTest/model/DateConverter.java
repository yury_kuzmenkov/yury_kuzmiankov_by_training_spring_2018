package junitTest.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {

    private SimpleDateFormat timeFormatter;

    public DateConverter() {
        timeFormatter = new SimpleDateFormat("dd-MM-yyyy");
    }

    public SimpleDateFormat getTimeFormatter() {
        return timeFormatter;
    }

    public void setTimeFormatter(SimpleDateFormat timeFormatter) {
        this.timeFormatter = timeFormatter;
    }

    /**
     *
     * @param date accept parameter for converted to Date object
     * @return Date object converted from String
     * @throws IllegalArgumentException
     */
    public Date getDate(String date) throws IllegalArgumentException {
        Date dateFormat = null;
        try {
            dateFormat = getTimeFormatter().parse(date);
        }catch (ParseException e){
            System.out.println(e.getMessage() + " ParserException " );
        }
        return dateFormat;
    }

    /**
     *
     * @param date accept parameter for converted to String object
     * @return String converted from Date
     * @throws IllegalArgumentException
     */
    public String getDateString(Date date) throws IllegalArgumentException {
        if (getTimeFormatter() == null){
            throw new IllegalArgumentException("pattern was not provided in constructor");
        }
        String dateFormate = getTimeFormatter().format(date);
        return dateFormate;
    }

}
