package junitTest.model;

import junitTest.dao.IDao;

import java.util.Map;

public class StringUtil {

   public String modifies(IDao dao){
       StringBuilder builder = new StringBuilder();
       for (Map.Entry<String, String> entry : dao.MAP.entrySet()) {
               builder.append(entry.getValue());
               builder.append("-");
       }
       if (builder.length() > 0){
           builder.setLength(builder.length()-1);
       }
       return builder.toString();
   }

}
