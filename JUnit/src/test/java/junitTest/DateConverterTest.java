package junitTest;

import junitTest.model.DateConverter;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DateConverterTest {

    @Test
    public void getDateStringTest(){
        DateConverter converter = mock(DateConverter.class);
        Date date = new Date();
        when(converter.getDateString(date)).thenThrow(new IllegalArgumentException());

        try {
            converter.getDateString(date);
        }catch (IllegalArgumentException e){
            System.out.println("test passed");
        }
    }

    @Test
    public void getDateTest(){
        DateConverter converter = mock(DateConverter.class);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        try {
            when(converter.getDate("13-04-2017")).thenReturn(simpleDateFormat.parse("13-04-2017"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            assertEquals(converter.getDate("13-04-2017"), simpleDateFormat.parse("13-04-2017"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
