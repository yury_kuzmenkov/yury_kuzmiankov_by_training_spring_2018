package junitTest;

import junitTest.dao.IDao;
import junitTest.model.DaoImpl;
import junitTest.model.StringUtil;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class StringUtilJUnitTest {

    @Test
    public void getValueTest(){
        IDao iDao = mock(DaoImpl.class);
        when(iDao.getV("day")).thenReturn("26");
        when(iDao.getV("month")).thenReturn("08");
        when(iDao.getV("year")).thenReturn("2019");

        assertEquals(iDao.getV("day"), "26");
        assertEquals(iDao.getV("month"), "08");
        assertEquals(iDao.getV("year"), "2019");
    }

    @Test
    public void putTest(){
        IDao iDao = mock(DaoImpl.class);
        when(iDao.putK("day", "25")).thenReturn("25");
        assertEquals("25", iDao.putK("day", "25"));
    }

    @Test
    public void StringUtilTest(){
        IDao iDao = new DaoImpl();
        StringUtil stringUtil = mock(StringUtil.class);

        when(stringUtil.modifies(iDao)).thenReturn("25-05-2018");
        assertEquals("25-05-2018", stringUtil.modifies(iDao));
    }

    @Test
    public void concatenatedTest(){
        StringUtil stringUtil = new StringUtil();
        IDao iDao = new DaoImpl();
        iDao.putK("testDay", "25");
        iDao.putK("testMonth", "05");
        iDao.putK("testYear", "2015");

        assertEquals("25-05-2015", stringUtil.modifies(iDao));
    }
}
