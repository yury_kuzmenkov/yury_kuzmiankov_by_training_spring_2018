package collection_test;

import collections.lfu.LFUCache;
import org.junit.Assume;
import org.junit.Test;

public class LFUCacheTest {

    @Test
    public void testIsFull(){
        final int capacity = 3;
        LFUCache<Integer, String> lfuCache =new LFUCache<Integer, String>(capacity);
        lfuCache.put(1, "One");
        Assume.assumeFalse(lfuCache.isFull());
    }

    @Test
    public void testIsClean(){
        final int capacity = 3;
        LFUCache<Integer, String> lfuCache =new LFUCache<Integer, String>(capacity);
        lfuCache.put(1, "One");
        lfuCache.clear();
        Assume.assumeFalse(lfuCache.isFull());
    }

}
