package collection_test;

import collections.lru.LRUCache;
import org.junit.Assume;
import org.junit.Test;


public class LRUCachTest {



    @Test
    public void testIsEmpty(){
        final int capacity = 3;
        LRUCache<Integer, String> lruCache = new LRUCache<Integer, String>(capacity);
        lruCache.put(1, "One");
        lruCache.put(2, "Two");
        lruCache.put(3, "Tree");
        Assume.assumeFalse(lruCache.isEmpty());
    }

    @Test
    public void testContains(){
        final int capacity = 3;
        LRUCache<Integer, String> lruCache = new LRUCache<Integer, String>(capacity);
        lruCache.put(1, "One");
        lruCache.put(2, "Two");
        Assume.assumeTrue(lruCache.containsKey(1));
    }
}
