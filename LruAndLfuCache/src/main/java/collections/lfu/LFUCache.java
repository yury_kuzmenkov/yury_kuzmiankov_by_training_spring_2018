package collections.lfu;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


public class LFUCache<K, V> {

    private LinkedHashMap<K, Node<V>> kvStore = new LinkedHashMap<K, Node<V>>();
    private static int initialCapacity = 3;

    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private final Lock r = rwl.readLock();
    private final Lock w = rwl.writeLock();

    public LFUCache(int initialCapacity) {
        this.initialCapacity = initialCapacity;
    }

// return put value
    public Node<V> put(K key, V data)
    {
        if(!isFull())
        {
            w.lock();
            try {
                Node<V> temp = new Node();
                temp.setData(data);
                temp.setFrequency(0);
                return kvStore.put(key, temp);
            }finally {
                w.unlock();
            }
        }
        else
        {
            w.lock();
            try {
                K entryKeyToBeRemoved = getKey();
                kvStore.remove(entryKeyToBeRemoved);
                Node temp = new Node();
                temp.setData(data);
                temp.setFrequency(0);
                return kvStore.put(key, temp);
            }finally {
                w.unlock();
            }

        }
    }

//return next remove key
    public K getKey()
    {
        K key = null;
        int minFreq = Integer.MAX_VALUE;
        r.lock();
        try {
            for(Map.Entry<K, Node<V>> entry : kvStore.entrySet())
            {
                if(minFreq > entry.getValue().getFrequency())
                {
                    key = entry.getKey();
                    minFreq = entry.getValue().getFrequency();
                }
            }
            return key;
        }finally {
            r.unlock();
        }
    }


    public V lookup(K key)
    {
        if(kvStore.containsKey(key))
        {
            r.lock();
            try {
                Node temp =  kvStore.get(key);
                temp.setFrequency(temp.getFrequency()+ 1);
                kvStore.put(key, temp);
                return (V)temp.getData();
            }finally {
                r.unlock();
            }
        }
        return null;
    }

    public  boolean isFull()
    {
        w.lock();
        try {
            if(kvStore.size() == initialCapacity){
                return true;
            }
            return false;
        }finally {
            w.unlock();
        }
    }

    public void clear(){
        w.lock();
        try {
            kvStore.clear();
        }finally {
            w.unlock();
        }
    }

}
