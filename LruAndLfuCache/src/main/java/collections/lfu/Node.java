package collections.lfu;

public class Node<V> {

    private V data;
    private int frequency;

    public Node() {
    }

    public V getData() {
        return data;
    }

    public void setData(V data) {
        this.data = data;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }
}
