package collections.lru;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruCacheImpl<K, V> extends LinkedHashMap<K, V> {

    private int capacity;

    public LruCacheImpl(int capacity) {
        super(capacity);
        this.capacity = capacity;
    }

    @Override
    protected synchronized boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() > this.capacity;
    }

    public static void main(String[] args) {
        System.out.println("ddd");
    }
}
